﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Robocode;

// The namespace with your initials, in this case FNL is the initials
namespace FP_AlphaTeam
{
    // The name of your robot is MyRobot, and the robot type is Robot
    class PseudoIntelligent : Robot
    {
        private Random _rand = new Random();
    
        // The main method of your robot containing robot logics
        public override void Run()
        {
            // -- Initialization of the robot --
            
            // Infinite loop making sure this robot runs till the end of the battle round
            int maxRandomTurningValue = 90;
            int minRandomTurningValue = 0;
            int randomTurningStep = 0;
            int randomTurningStepSize = 0;
            int maxRandomTurningSteps = 1;
            var moveAheadDistance = 50;

            while (true)
            {
                if (randomTurningStep < maxRandomTurningSteps)
                {
                    TurnRight(randomTurningStepSize);
                    randomTurningStep++;
                }
                else
                {
                    randomTurningStepSize = _rand.Next(minRandomTurningValue, maxRandomTurningValue) / maxRandomTurningSteps;
                    randomTurningStep = 0;
                }

                Ahead(moveAheadDistance);
            }
        }

        // Robot event handler, when the robot sees another robot
        public override void OnScannedRobot(ScannedRobotEvent e)
        {
            TurnRight(e.Bearing);
            if (e.Distance < 200)
                Fire(1);
            else
                Ahead(e.Distance - 100);
        }

        public override void OnHitWall(HitWallEvent e)
        {
            TurnRight(_rand.Next(90, 180));
            Ahead(1);
        }
    }
}